To install:

```
cd
git clone https://gitlab.com/spacyricochet/dotfiles.git .dotfiles
ln -s ~/.dotfiles/powerline-shell.json ~/.powerline-shell.json
ln -s ~/.dotfiles/zshrc ~/.zshrc
ln -s ~/.dotfiles/nethackrc ~/.nethackrc
ln -s ~/.dotfiles/gitconfig .gitconfig
```
